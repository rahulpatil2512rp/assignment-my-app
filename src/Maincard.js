import logo from "./logo.svg";
import React, { Component } from 'react';

import "./App.css";
import reportWebVitals from "./reportWebVitals";
import "bootstrap/dist/css/bootstrap.min.css";
import FixedIncome from "./Components/FixedIncome";
import Sectors from "./Components/Sectors";
import MarketNews from "./Components/MarketNews";
import Performance from "./Components/Performance";
import USequityFactors from "./Components/USequityFactors";
import USequityMarkets from "./Components/USequityMarkets";
import GlobalYields from "./Components/GlobalYields";
import Currencies from "./Components/Currencies";
import GlobalMarkets from "./Components/GlobalMarkets";
import Commodities from "./Components/Commodities";
import Card from "react-bootstrap/Card";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, fapensquare } from "@fortawesome/free-solid-svg-icons";


class Maincard extends React.Component {
  render()
{
  return (
    <div className="maincard" style={{position:"absolute", marginLeft : "23rem",   width: "71.5rem"  }}>
      <Card style={{ backgroundColor: "white", height: "56rem" }}>
        <USequityMarkets />
        <Sectors />
        <FixedIncome />
        <MarketNews />
        <Performance />
        <GlobalYields />
        <USequityFactors />
        <Currencies />
        <GlobalMarkets />
        <Commodities />
      </Card>
    </div>
  );
}
}
export default Maincard;

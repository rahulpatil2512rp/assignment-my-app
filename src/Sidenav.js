import "./App.css";
import React, { Component } from 'react';

import reportWebVitals from "./reportWebVitals";
import { AiTwotoneSetting } from "react-icons/ai";
import { GiHamburgerMenu, GiEarthAmerica } from "react-icons/gi";
import { FaNewspaper } from "react-icons/fa";
import { ImFire } from "react-icons/im";
import { BiBarChartSquare } from "react-icons/bi";
import { RiMenuFoldLine } from "react-icons/ri";


class Sidenav extends React.Component {
  render()
{
  return (
    <div className="sidenav">
      <div
        class="card"
        style={{
          width: "23rem",
          height : "100vh",
          backgroundColor: "#282c34",
        }}
      >
        <div class="card-header">
          <RiMenuFoldLine
            style={{
              color: "white",
              fontSize: "35px",
              position: "absolute",
              right: "0rem",
            }}
          />
        </div>
        <div style = {{margin : "10px", padding : "10px", width : "23.5rem"}}>
          <div style={{ marginTop: "2rem", right :"0rem" }}>
            <div  className="hoverlist"
              style={{
                color: "white",
                padding: "10px",
                width: "100%",
                fontWeight: "bold",
                fontSize: "16px",
                left: "0rem",
              }}
            >
              <GiEarthAmerica
                style={{ fontSize: "22px", marginLeft: "1rem" }}
              />
              <label style={{ color: "white", marginLeft: "2rem" }}>
                {" "}
                Today's Markets
              </label>
              <button 
                style={{
                  border: "1px solid rgb(206, 205, 205)",
                  position: "absolute",
                  right: "0.5rem",
                  color: "gray",
                  width: "4rem",
                  padding: "2px 10px",
                  borderRadius: "5px",
                  fontSize: "14px",
                }}
              >
                Now
              </button>
            </div>
            <div  className="hoverlist"
              style={{
                color: "white",
                padding: "10px",
                width: "100%",
                fontWeight: "bold",
                fontSize: "16px",
              }}
            >
              <FaNewspaper style={{ fontSize: "22px", marginLeft: "1rem" }} />
              <label style={{ color: "white", marginLeft: "2rem" }}>
                Top News
              </label>
              <button
                style={{
                  border: "1px solid rgb(206, 205, 205)",
                  position: "absolute",
                  width: "4rem",
                  color: "gray",
                  padding: "2px 10px",
                  borderRadius: "5px",
                  fontSize: "14px",
                  right : "0.5rem"
                }}
              >
                TOP
              </button>
            </div>
            <div className="hoverlist"
              style={{
                color: "white",
                padding: "10px",
                width: "100%",
                fontWeight: "bold",
                fontSize: "16px",
              }}
            >
              <ImFire style={{ fontSize: "22px", marginLeft: "1rem" }} />
              <label style={{ color: "white", marginLeft: "2rem" }}>
                Market Movers
              </label>
              <button
                style={{
                  border: "1px solid rgb(206, 205, 205)",
                  position: "absolute",
                  right: "0.5rem",
                  color: "gray",
                  width: "4rem",
                  padding: "2px 10px",
                  borderRadius: "5px",
                  fontSize: "14px",
                }}
              >
                MOV
              </button>
            </div>
            <div  className="hoverlist"
              style={{
                color: "white",
                padding: "10px",
                width: "100%",
                fontWeight: "bold",
                fontSize: "16px",
              }}
            >
              <BiBarChartSquare
                style={{ fontSize: "22px", marginLeft: "1rem" }}
              />
              <label style={{ color: "white", marginLeft: "2rem" }}>
                My Watchlists
              </label>
              <button
                style={{
                  border: "1px solid rgb(206, 205, 205)",
                  position: "absolute",
                  right: "0.5rem",
                  color: "gray",
                  padding: "2px 10px",
                  width: "4rem",
                  borderRadius: "5px",
                  fontSize: "14px",
                }}
              >
                MYW
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
}

export default Sidenav;

import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";


class GlobalMarkets extends React.Component {
  render()
{
  return (
    <div className="end-2" style={{position:"absolute", marginLeft : "54rem"}}>
      <label className="card-label">GLOBAL MARKETS</label>
      <FaExternalLinkSquareAlt style={{ position : "absolute",top:"10px" ,right:"0.5rem",fontSize : "16px"}}/>

      <Card style={{  width: "16rem",
            marginLeft: "10px",
            boxShadow: "1px 1px 10px #9E9E9E",
            height : "29rem"
            }}>
        <div className="cardheader">
            <div className="cardcontent">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label class="form-check-label" for="flexCheckDefault">
                  Broad Markets
                </label>
                <label className="percentage">
                  %
                </label>
              </div>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Emerging
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
              Frontier
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Developed
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardheader">
          <div className="cardcontent">
          
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
               Developed Markets
              </label>
              <label className="percentage">
                  %
                </label>
            </div>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
            Japan
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                France
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
              Australia
                </label>
                <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
              United Kingdom
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Germany
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          
          <div className="cardheader">
            <div className="cardcontent">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label class="form-check-label" for="flexCheckDefault">
                Emerging Markets
                </label>
                <label className="percentage">
                  %
                </label>
              </div>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Brazil
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                 India
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                South Korea
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                 Russia
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                 China
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                 Mexico
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>
      </Card>
    </div>
  );
}
}

export default GlobalMarkets;

import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";

class Currencies extends React.Component {
  render()
{
  return (
    <div className="end-1" style={{position:"absolute", marginLeft : "54rem"}}>
      <label className="card-label">CURRENCIES</label>
      <FaExternalLinkSquareAlt style={{ position : "absolute",top:"10px" ,right:"0.5rem",fontSize : "16px"}}/>

      <Card style={{  width: "16rem",
            marginLeft: "10px",
            boxShadow: "1px 1px 10px #9E9E9E", }}>
        <div className="cardheader">
            <div className="cardcontent">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label class="form-check-label" for="flexCheckDefault">
                  Major Indices & ETFs
                </label>
                <label className="percentage">
                  %
                </label>
              </div>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Bitcoin
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Japanese yen
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Euro
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                British Pound
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>
      </Card>
    </div>
  );
}
}

export default Currencies;

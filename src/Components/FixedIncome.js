import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";


class FixedIncome extends React.Component {
  render()
{
  return (
    
    <div className="start-3">
      <label className="card-label">FIXED INCOME</label>
      <FaExternalLinkSquareAlt style={{ top:"10px" ,marginLeft:"4.5rem",fontSize : "16px"}}/>

      <div class="d-flex flex-row">
        <Card
          style={{
            width: "14rem",
            marginLeft: "10px",
            boxShadow: "1px 1px 10px #9E9E9E",
            height : "14rem"

          }}
        >
          <div className="cardheader">
            <div className="cardcontent">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label class="form-check-label" for="flexCheckDefault">
                  Govt. Credits ETFs
                </label>
                <label className="percentage">
                  %
                </label>
              </div>
            </div>
          </div>


          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                T.I.P.S
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                U.S. Treasuries
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
              Muncipals
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardheader">
            <div className="cardcontent">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label class="form-check-label" for="flexCheckDefault">
                  Corp Credits ETFs
                </label>
                <label className="percentage">
                  %
                </label>
              </div>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Convertibles
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Russel 2000
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                CBOE VIX
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>
        </Card>
      </div>
      
    
    </div>
  );
}
}

export default FixedIncome;

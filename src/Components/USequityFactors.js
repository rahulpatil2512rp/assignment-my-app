import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";


class USequityFactors extends React.Component {
  render()
{
  return (
    <div className="mid-3" style={{position:"absolute", marginLeft : "16rem"}}>
      <label className="card-label">US EQUITY FACTORS</label>
      <FaExternalLinkSquareAlt style={{ position : "absolute",top:"10px" ,right:"0.5rem",fontSize : "16px"}}/>


      <Card
        style={{
          width: "16rem",
          marginLeft: "10px",
          boxShadow: "1px 1px 10px #9E9E9E",
          height: "14rem",
        }}
      >
        <div className="cardheader">
          <div className="cardcontent">
            <label class="form-check-label" for="flexCheckDefault">
              1-Day Performance
            </label>
          </div>
        </div>

        <div
          style={{
            marginLeft: "4rem",
            fontSize: "14px",
            color: "#000",
            fontWeight: "500",
          }}
        >
          <label style={{ marginLeft: "1rem" }}>Value</label>
          <label style={{ marginLeft: "1.5rem" }}>Core</label>
          <label style={{ marginLeft: "1.5rem" }}>Growth</label>
        </div>

        <div
          style={{
            marginLeft: "1rem",
            fontSize: "14px",
            color: "#000",
            fontWeight: "500",
          }}
        >
          <label style={{ marginLeft: "0rem" }}>Large</label>
          <label
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#ffb1ab",
            }}
          >
            -0.1%
          </label>
          <label
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#abfdb7",
            }}
          >
            0.1%
          </label>
          <label
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#50fb69",
            }}
          >
            0.3%
          </label>
        </div>

        <div
          style={{
            marginLeft: "1rem",
            fontSize: "14px",
            color: "#000",
            fontWeight: "500",
          }}
        >
          <label style={{ marginLeft: "0rem" }}>Med</label>
          <label
            style={{
              marginLeft: "1.3rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#ffb1ab",
            }}
          >
            -0.5%
          </label>
          <label
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#50fb69",
            }}
          >
            0.5%
          </label>
          <label
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#cbfed2",
            }}
          >
            0.5%
          </label>
        </div>

        <div
          style={{
            marginLeft: "1rem",
            fontSize: "14px",
            color: "#000",
            fontWeight: "500",
            
          }}
        >
          <label style={{ marginLeft: "0rem" }}>Small</label>
          <label
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#cbfed2",
            }}
          >
            0.5%
          </label>
          <label
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#cbfed2",
            }}
          >
            0.5%
          </label>
          <label
            style={{
              marginLeft: "1rem",
              height: "3rem",
              width: "3rem",
              borderRadius: "5px",
              backgroundColor: "#ffb1ab",
              
            }}
          >
            -0.5%
          </label>
        </div>
      </Card>
    </div>
  );
}
}

export default USequityFactors;

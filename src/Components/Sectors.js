import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";


class Sectors extends React.Component {
  render()
{
  return (
      <div className="start-2">
        <label className="card-label">U.S. EQUITY SECTORS</label>
        <FaExternalLinkSquareAlt style={{ top:"10px" ,marginLeft:"2rem",fontSize : "16px"}}/>
        <div class="d-flex flex-row">
        <Card
          style={{
            width: "14rem",
            marginLeft: "10px",
            boxShadow: "1px 1px 10px #9E9E9E",
            height : "22rem", 

          }}
        >
          <div className="cardheader">
            <div className="cardcontent">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label class="form-check-label" for="flexCheckDefault">
                 S&P Sectors ETFs
                </label>
                <label className="percentage">
                  %
                </label>
              </div>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Communications
              </label>
              <label className="loss">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
               Const. Discretionary
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Health Care
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Const. Staples
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Utilities
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                 Energy
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Technology
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
               Materials
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Industries
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          

          

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Financials
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Real Estate
              </label>
              <label className="loss">
                  -0.8%
                </label>
            </div>
          </div>
        </Card>
      </div>
      </div>
  );
}
}

export default Sectors;

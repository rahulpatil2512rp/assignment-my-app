import Card from "react-bootstrap/Card";
import React, { Component } from 'react';

import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";


 
class USequityMarkets extends React.Component {
  render()
  {
  return (
    <div className="start-1">
      <label className="card-label">U.S. EQUITY MARKETS</label>
      <FaExternalLinkSquareAlt style={{ top:"10px" ,marginLeft:"2rem",fontSize : "16px"}}/>

      <div class="d-flex flex-row">
        <Card
          style={{
            width: "14rem",
            marginLeft: "10px",
            boxShadow: "1px 1px 10px #9E9E9E",
          }}
        >
          <div className="cardheader">
            <div className="cardcontent">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                 
                />

                <label class="form-check-label" for="flexCheckDefault">
                  Major Indices & ETFs
                </label>
                <label className="percentage">%</label>
              </div>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Russel 2000
              </label>
              <label className="profit">
                  0.8%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Nasdaq 100
              </label>
              <label className="profit">
                  0.4%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                S&P 500
              </label>
              <label className="loss">
                 -0.4%
                </label>
            </div>
          </div>

          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Dow Jones
              </label>
              <label className="loss">
                 -1.1%
                </label>
            </div>
          </div>

          <div className="cardheader">
            <div className="cardcontent">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="flexCheckDefault"
                />
                <label class="form-check-label" for="flexCheckDefault">
                  Volattility Index
                </label>
                <label className="percentage">
                  %
                </label>
              </div>
            </div>
          </div>

          <div class="form-check">
            <div className="cardcontent">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                CBOE VIX
              </label>
              <label className="loss">
                  -1.1%
                </label>
            </div>
          </div>
        </Card>
      </div>
    </div>
  );
}
}

export default USequityMarkets;

import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import { FaShareAlt, FaTags, FaExternalLinkSquareAlt } from "react-icons/fa";
import { BiVolumeMute } from "react-icons/bi";
import { ImTarget } from "react-icons/im";
import { BsSquare } from "react-icons/bs";
import "./style.css";


class Performance extends React.Component {
  render()
{
  return (
    <div className="mid-2" style={{position:"absolute", marginLeft : "16rem"}}>
      <div class="d-flex justify-content-center">
        <div>
          <label className="card-label">NORMALIZED PERFORMANCE</label>
          <FaExternalLinkSquareAlt
            style={{
              position: "absolute",
              top: "10px",
              right: "0.5rem",
              fontSize: "16px",
            }}
          />
          <FaShareAlt
            style={{
              position: "absolute",
              top: "10px",
              right: "8rem",
              fontSize: "16px",
            }}
          />
          <label
            style={{
              position: "absolute",
              top: "7px",
              fontWeight: "700",
              right: "5rem",
              fontSize: "13px",
            }}
          >
            SHARE
          </label>

          <Card
            className="card-center"
            style={{
              width: "36rem",
              height: "28rem",
              marginLeft: "10px",
              boxShadow: "1px 1px 10px #9E9E9E",
            }}
          >
            <div className="cardheader">
              <div className="cardcontent">
                <div class="form-check">
                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    {" "}
                    1D
                  </button>
                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    5D
                  </button>
                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    {" "}
                    1M
                  </button>
                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    3M
                  </button>
                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    {" "}
                    6M
                  </button>
                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    YTD
                  </button>

                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    1Y
                  </button>

                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    {" "}
                    3Y
                  </button>
                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    5Y
                  </button>
                  <button
                    style={{
                      marginLeft: "1rem",
                      fontWeight: "bold",
                      fontSize: "14px",
                    }}
                  >
                    {" "}
                    10Y
                  </button>
                </div>
              </div>
            </div>
            <div>
              <h1>STOCK CHAT HERE</h1>
            </div>
            <div className="cardheader">
              <div className="performancecardFooter">
                <div class="form-check">
                  <label
                    style={{
                      marginLeft: "21rem",
                      fontWeight: "bold",
                      fontSize: "18px",
                    }}
                  >
                    <BiVolumeMute />
                  </label>
                  <label
                    style={{
                      marginLeft: "2rem",
                      fontWeight: "bold",
                      fontSize: "18px",
                    }}
                  >
                    {" "}
                    <ImTarget />
                  </label>
                  <label
                    style={{
                      marginLeft: "2rem",
                      fontWeight: "bold",
                      fontSize: "18px",
                    }}
                  >
                    {" "}
                    <BsSquare />
                  </label>
                  <label
                    style={{
                      marginLeft: "2rem",
                      fontWeight: "bold",
                      fontSize: "21px",
                    }}
                  >
                    {" "}
                    <FaTags />
                  </label>
                </div>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </div>
  );
}
}
export default Performance;

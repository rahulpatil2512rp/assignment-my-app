import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";


class GlobalYields extends React.Component {
  render()

{
  return (
    <div className="mid-4" style={{position:"absolute", marginLeft : "34.5rem"}}>
      <label className="card-label">GLOBAL YIELDS</label>
      <FaExternalLinkSquareAlt style={{ position : "absolute",top:"10px" ,right:"0.5rem",fontSize : "16px"}}/>


      <Card
        style={{
          width: "17rem",
          marginLeft: "10px",
          boxShadow: "1px 1px 10px #9E9E9E",
          height: "14rem",
        }}
      >
        <div className="cardheader">
          <div className="cardcontent">
            <div class="form-check">
              
              <label style={{
                marginLeft: "8rem",
                fontWeight: "bold",
                fontSize: "14px",
              }} >
              5Y
              </label>
              <label style={{
                marginLeft: "3rem",
                fontWeight: "bold",
                fontSize: "14px",
              }}> 10Y</label>
            </div>
          </div>
        </div>

       
        <div>
          <img
            src="https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/1200px-Flag_of_the_United_States.svg.png"
            style={{
              height: "22px",
              width: "35px",
              borderRadius: "5px",
              marginLeft: "5px",
            }}
          />
          <label
            style={{
              marginLeft: "0.5rem",
              fontWeight: "500",
              fontSize: "16px",
            }}
          >
            US
          </label>
          <label
            style={{ marginLeft: "5rem", fontWeight: "500", fontSize: "15px" }}
          >
            0.877%
          </label>
          <label
            style={{ marginLeft: "1rem", fontWeight: "500", fontSize: "15px" }}
          >
            1.725%
          </label>
        </div>

        <div>
          <img
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASIAAACuCAMAAAClZfCTAAAAElBMVEUAAAD/zgDdAADnAADaAAD/2AAtsSEoAAAA+ElEQVR4nO3QMQGAMAAEsYeCf8tIuI0pkZANAAAAAAAAAAAAAAAAAAAAgB8dwm6CoqQoKUqKkqKkKClKipKipCgpSoqSoqQoKUqKkqKkKClKipKipCgpSoqSoqQoKUqKkqKkKClKipKipCgpSoqSoqQoKUqKkqKkKClKipKipCgpSoqSoqQoKUqKkqKkKClKewh7CbsIipKipCgpSoqSoqQoKUqKkqKkKClKipKipCgpSoqSoqQoKUqKkqKkKClKipKipCgpSoqSoqQoKUqKkqKkKClKipKipCgpSoqSoqQoKUqKkqKkKClKipKipCgpSoqSoqQoKUofMGTNC8HkSxoAAAAASUVORK5CYII="
            style={{
              height: "22px",
              width: "35px",
              borderRadius: "5px",
              marginLeft: "5px",
            }}
          />
          <label
            style={{
              marginLeft: "0.5rem",
              fontWeight: "500",
              fontSize: "16px",
            }}
          >
            DE
          </label>
          <label
            style={{ marginLeft: "5rem", fontWeight: "500", fontSize: "15px" }}
          >
            0.877%
          </label>
          <label
            style={{ marginLeft: "1rem", fontWeight: "500", fontSize: "15px" }}
          >
            1.725%
          </label>
        </div>

        <div>
          <img
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAT4AAACfCAMAAABX0UX9AAAAdVBMVEX////IEC4BIWnFABjrvcEAAF8AHmgAAGHICSvKKD3HACabobkACWH02t2prsLGACHEAADcgYvUWWf78fLFAA7EAAYAHWkAGmj5+vwAFWbFABDtxcjKITjVYm/fj5fSUWDz1diUm7Te4OgAAFbSSVv55+rbdYEsZ9eAAAAFlklEQVR4nO3dWXfaQAwF4AktkIQszgLZt2b5/z+xSU/LYmysmXslTU51n/IAnvEHAS+DlB6O01CuZzfjyUgvR7PVWAf7ezuyf7B65OxIcUqT8c3sejnUxWPXrA6bp+d02JwPA6aTK0XA6vgm46uT1UAXZ81pF94ft+UffoCV8WXgffGJARdKgFXxTcaLYbz50qul6QFYEV8m3j8+V8Bq+Np48yG8FZ8YcEIHrITvc8dy8db53ACr4CvC2+RzAqyArxCvzecC6M7XxruU4m3zOQA68wF4KZ3uOii0AXTlg/B6n2AJ6MgH4lUB6MZHwKsA0ImPhOcO6MInwhPtvzegAx8Zb8dGRG9fCNCcj4i3IP3/A4DGfDy8k0XifYAWA5ryEfE+N5WY30CFgIZ8XLzRKIk3qgdoxsfG+8vnDGjEx8db8rkCmvBp4K3xAde8UEADPh28DT43QHU+LbwWHxVQflNJma99A4h5dpW2B5vk3arDAVX5RHfPik9Nt/gK7nX2DyhbmaDI114xAOB1vhk6+ISAvKUdanzaeD18mes8UEAlPot96OEzBVThs5l/L58hoAKf1dx38Fl8dujw2X127+QzAiTzWX7xDfBpHzfx+WyPWwf5DACJfNYH/QI+zXNGLp/9ObuITxmQxOdxwUPIpwpI4fO5WiTmUwQk8Hld7M3gUwOE+fyulGfxfU10xAcE+Yh4o9z7NJl8KoAQnydeAZ8CIMDni1fERwcs5vPGK+QjA07L+KbueMV8VMCjlxK+lyPW+MV4AB/zc+dXCd/ak/zWJgJ81OWt+XwV4IF8CoDZfL7rskE+OmAmn/evAtIUzvh1uv67/7PbH9uZN2/PfL73j6bpGOxWgHc9m76O8X0X7dPwZK4FjxFtKYtPtsnu2QDPrTgFXx2RVYIPSvBBCT4owQcl+KAEH5Tgg/Ld+H5Wlbv73Xz3d94z3EzqOGf0zE69Tz/v+bXSUYgkIk/wQQk+KMEHJfigBB+U4IMSfFCCD0rwQQk+KMEHJfigBB+U4IMSfFCCD0rwQQk+KMEHJfigBB+U4IMSfFCCD0rwQQk+KMEHJfigxAorKLG+D4r36sx2vtvq0soSfFCCD0rwQQk+KMEHJfigBB+U/5IPqQyw+VQKn6SyAqmWAbuSRl+Oz7sqbDTNx/vGwyh8z2/NvOsM9exi+RBWJQ1uHZd+vOZwW+P0cm2HiHxf4827xrtdH49Sx6UqPOJnnw0gsYYVAY/61dEDeMkEpFVQQ/DQCmp+gED9Phbeybisft+YNT4AWFw9UoYnevUp1SN9AAtrlxLxaLVLPQCLKudS8YiVc+0BC+o2k/GodZutAbOrhtPxyFXDbQGBmvUkPHrNekvA4o4JNDyFjgl2gIX9OjLx5jsnqNCvwwqwqFsMFU+pW4wNoLBX0UIPT61XEQQo7O6a3SkrE68RvKpqnbIyAef5gII+bVe6eKp92iBAQXfXrC6BmZNphK+mapdAXcCMHpVKeOo9KjUBxR1SITznDql6cxf251XEM+nPqzV/UXdoVTyj7tA6+yDoTZ45cPa3l1VvcghQ3pvcGE+Rj3rY1Qm4xWd94KnLRz3o79if1B7M+rRHm0/3lDNtDuRy2UeZT/OCR8ofhH7RUZ1PDzDlDqBwyduAT+tib3LHM+LTAUzueGZ8GoDJHc+Qjw+Y3PFM+diACTjDYC20MeWjAi6Gt6O+Ssmajwnoj+fAp7Kw0wnPhc8E0GJ5qxefOqARnhufKqAZniOfGqAhnisfCHjqj+fMhwH647nzIYBSvOGFPuVx5ysH9Mergq8U0B+vEr4ywFw8+Q0geSrhK7lFO4QnX+hTnmr48gH98ariywXcejv24slWDJSkKr68pR3+eNXx5QCm56cuvL39x/Xf/d8o4lXI9wV4I6nP8PAbCPZ0ewGb5p0AAAAASUVORK5CYII="
            style={{
              height: "22px",
              width: "35px",
              borderRadius: "5px",
              marginLeft: "5px",
            }}
          />
          <label
            style={{
              marginLeft: "0.5rem",
              fontWeight: "500",
              fontSize: "16px",
            }}
          >
            GB
          </label>
          <label
            style={{ marginLeft: "5rem", fontWeight: "500", fontSize: "15px" }}
          >
            0.877%
          </label>
          <label
            style={{ marginLeft: "1rem", fontWeight: "500", fontSize: "15px" }}
          >
            1.725%
          </label>
        </div>

        <div>
          <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQDNqWsPXQ-JMj4mBpRqV0VcUFTHJDGn59tw&usqp=CAU"
            style={{
              height: "22px",
              width: "35px",
              borderRadius: "5px",
              marginLeft: "5px",
            }}
          />
          <label
            style={{
              marginLeft: "0.5rem",
              fontWeight: "500",
              fontSize: "16px",
            }}
          >
            ES
          </label>
          <label
            style={{ marginLeft: "5rem", fontWeight: "500", fontSize: "15px" }}
          >
            0.877%
          </label>
          <label
            style={{ marginLeft: "1rem", fontWeight: "500", fontSize: "15px" }}
          >
            1.725%
          </label>
        </div>

        <div>
          <img
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAFVBMVEX///8AkkbOKzcAjz5zt4zef4TNIC6B8V5lAAAA/klEQVR4nO3QSQ0AIAADsHH6l4yKPUhaCc2oWTs9586aOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHixIkTJ06cOHHy4ckD5KrN4eD2boIAAAAASUVORK5CYII="
            style={{
              height: "22px",
              width: "35px",
              borderRadius: "5px",
              marginLeft: "5px",
            }}
          />
          <label
            style={{
              marginLeft: "0.5rem",
              fontWeight: "500",
              fontSize: "16px",
            }}
          >
           IT
          </label>
          <label
            style={{ marginLeft: "5rem", fontWeight: "500", fontSize: "15px" }}
          >
           0.877%
          </label>
          <label
            style={{ marginLeft: "1rem", fontWeight: "500", fontSize: "15px" }}
          >
            1.725%
          </label>
        </div>

      
        <div>
          <img
            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAilBMVEXeKRD/3gD/4QDdHxD/4wD4uwX/5ADeJRDdHBD/5gDdFhD/3QDthAr+1wD/2wDmYA3yngj7ywP0qAffLw/jTA7lWA32sQb5wQTcCBHgOA/8zwH6xgTkTw3xlgjvjgnoawziRA7regv/7gD3twb1rgbqdAvsgQrnYw3iPw7uign1sAfqbwvzoQfwmQktJ6URAAAFPElEQVR4nO3ca3eqOhAGYEkik0RBEVAu4qW21V78/3/vRFFrW7XgtqclfZ9Pey+rC8bMZBKQVgsAAAAA/jiinz6CX4fHYwRlh5eR4JE3Q0xK4UOoTSwo9PwUMSnxwisizXkuMoRkh1qB6jzHQ5dlyU8fy6/Bp8IRjDlshJgc6KFwDDZKE9uzp/L50cpztkFxh7Hm33lIP42iqkGRse+UhAoev/Wgfph+jasFhUc+KyPi+tlwUvFdjZR4hazyd5RmzDCpM3iYaS0tDglFqtLcSmGuRNbOzbwTa4vjsSELIaqsX6hYdOMwCR2nU7kANVWSMVEpeUKTLtTSE+WHlseEIlMg6jSmOvOtnoYNOdn0YTWygfd829tYbYqmUy15duSkxh83EY07m8Y013Xec/Y/dpATd9OFda7dOUtn9hUXPdq2pm73ynygUbQLijWbtBSVKxg2qpM8R/TI244Urpe29Pqy65aLumv7ML1WWcq5jheZLV3LLnWuTx4TVJG1egum5pbMRjTbL/6vTZ7N3hvzmWC5LfXkkDr12rbjT1iVUXWntsw/ss32Iblq7HPda/9bjf5hxD+SqXcYJuasPr3+xVdPfJmrMqhswZuYOmF/1P5odBgmZub59Gp79HDpRCnO7xTb1+hFEztaor4r2AfOkY+vKf/h4kChWW8+aQfbesJMUBo5UvQ0E05lor36osIQcam5WVezLHDU3aKJMWnJdOh+HYxy0DhP1TZct5PxrDWLH58njZx6SHbf58vZQZLFFScSijpikGyHzPce+/fR46zCUFHPX805B9TyVdPbNa4H6qu8cec1rn0mXr+xI+QgefEvllqRf1Vc39ELGxZ/cjU6HxQm+vUmVVpZEJJNqe2fK7XCm9a9oGVFSMxp6OXpVsVtp5Ys+a9gWpUTI6XzZPtVz4vk9MRA6Vh/r81FenAiJuKlka3orejOqQo7aH6rcT1+KnUcx/u7FXZzc9LJmAhrthCvwL1TIXHENX26JXWZ945mYvH2b5bVX8/xJzsSTvcPqcOc7v3bUpn1aidPktlxBZBn+7HhBrE+2lWpnzwUKitmq7fUUfehGRhyHLiH5Kn7WXPXsSEm+9Rh7KVs5jntd1VE3eQxzZ8VrR4F23Gi3nZKSL90WM3kISm11onvsHayQek3He7/geLyppPieMUn03yTPyyoOvNQr1j3X18X2zd5vrrLm/xTJ12Y1BH+8v29epQUm1mZVZ5F+IQpUU7kzGGiaPKamlomdcSJnZKk5wnHnFvVD9JRez+LM2/a6EJLkflyu6e+VR7eC5ZX/yROT7tfr+SzZjducqKCM5dvSHY7qsZdF5Q8bmcwUTT8nlkKBuHZyVOPg3WNr1zfi201CZo9G9NqfumyJw+7NT6rlTHhjdQ1a4Lf5fLhU42z4z2hRqEumLpvdIm9JTm5W5tirce5Z8OVr5vg+XxbXLl+nTc8eW4mjfb1WIc/eiC/CRKmhDh8xteIys5+vawLr+FN683QKt3+Hl8uFZ7hsBfmg2UiecpEG/3ZDu+5KpuEGWPPiMme7rtMKOaI9d++/eAYheU2LmuPWzY/raAW3itvRmAqmERN3mm8IR7trzILpRbWP7WgCgrz3fNPVDacR1jgGHIonM1FVDGIQt3IX2HcnC6UcnMzVNwH1JISn7P7eSql/7dv3HlvpjcJw2PFlojJzi5fkoFrx/0mN0QtD49K/YhP0Zl8gogAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADAe/8B/Wo7/fzKIMAAAAAASUVORK5CYII="
            style={{
              height: "22px",
              width: "35px",
              borderRadius: "5px",
              marginLeft: "5px",
            }}
          />
          <label
            style={{
              marginLeft: "0.5rem",
              fontWeight: "500",
              fontSize: "16px",
            }}
          >
           CN
          </label>
          <label
            style={{ marginLeft: "5rem", fontWeight: "500", fontSize: "15px" }}
          >
            0.877%
          </label>
          <label
            style={{ marginLeft: "1rem", fontWeight: "500", fontSize: "15px" }}
          >
            1.725%
          </label>
        </div>
      </Card>
    </div>
  );
}
}

export default GlobalYields;

import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";



class MarketNews extends React.Component {
  render(){
  return (
    
    <div className="mid-1" style={{position:"absolute", marginLeft : "16rem"}}>
      <div class="d-flex justify-content-center">
        <div >
          <label className="card-label">MARKET NEWS</label>
          <FaExternalLinkSquareAlt style={{ position : "absolute",top:"10px" ,right:"0.5rem",fontSize : "16px"}}/>

          <Card
            className="card-center"
            style={{
              width: "36rem",
              height: "5rem",
              marginLeft: "10px",
              boxShadow: "1px 1px 10px #9E9E9E",
            }}
          >
            <div className="newscardcontent">
              <div class="form-check">
                
                <label class="form-check-label" for="flexCheckDefault">
                  Magnitude 7.2 Quake Hits Northern Japan; Tsunami Advisor...
                </label>
                <label className="timestamp">
                03:00 PM
                </label>
              </div>
            </div>

            <div className="newscardcontent">
              <div class="form-check">
                
                <label class="form-check-label" for="flexCheckDefault">
                  To Buy a Home in the Pandemic, Relatives Team Up To Bids
                </label>
                <label className="timestamp">
                03:00 PM
                </label>
              </div>
            </div>

            <div className="newscardcontent">
              <div class="form-check">
                
                <label class="form-check-label" for="flexCheckDefault">
                  Credit Suisses Plans To Triple China Headcount in Major Push
                </label>
                <label className="timestamp">
                  03:00 PM
                </label>
              </div>
            </div>
          </Card>
        </div>
      
    </div>
    </div>
  );
}
}
export default MarketNews;

import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from 'react-bootstrap/Button';
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";



class Markets extends React.Component {
  render()
{
  return (
    <div className="App">
                  <label className="card-label">GLOBAL MARKETS</label>
                  <FaExternalLinkSquareAlt style={{ position : "absolute",top:"10px" ,right:"0.5rem",fontSize : "16px"}}/>


      <Card style={{ width: "18rem" }}>
        <Card.Body>
          <Card.Title>Card Title</Card.Title>
          <Card.Text>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>
    </div>
  );
}
}

export default Markets;

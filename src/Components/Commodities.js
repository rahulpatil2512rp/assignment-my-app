import logo from "../logo.svg";
import React, { Component } from 'react';

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import "./style.css";
import { FaBeer,FaTags,FaExternalLinkSquareAlt } from "react-icons/fa";



class Commodities extends React.Component {
  render()
{
  return (
    <div className="end-3" style={{position:"absolute", marginLeft : "54rem"}}>
      <label className="card-label">COMMODITIES</label>
      <FaExternalLinkSquareAlt style={{ position : "absolute",top:"10px" ,right:"0.5rem",fontSize : "16px"}}/>


      <Card
        style={{
          width: "16rem",
          marginLeft: "10px",
          boxShadow: "1px 1px 10px #9E9E9E",
          height : "10rem" 
        }}
      >
        <div className="cardheader">
          <div className="cardcontent">
            <div class="form-check">
              <input
                class="form-check-input"
                type="checkbox"
                value=""
                id="flexCheckDefault"
              />
              <label class="form-check-label" for="flexCheckDefault">
                Broad Markets
              </label>
              <label className="percentage">
                 %
                </label>
              
            </div>
          </div>
        </div>

        <div className="cardcontent">
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="flexCheckDefault"
            />
            <label class="form-check-label" for="flexCheckDefault">
              Natural Gas
            </label>
            <label className="profit">
                  0.8%
                </label>
          </div>
        </div>

        <div className="cardcontent">
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="flexCheckDefault"
            />
            <label class="form-check-label" for="flexCheckDefault">
              Crude Oil
            </label>
            <label className="profit">
                  0.8%
                </label>
          </div>
        </div>

        <div className="cardcontent">
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="flexCheckDefault"
            />
            <label class="form-check-label" for="flexCheckDefault">
              Brent Oil
            </label>
            <label className="loss">
                  -0.8%
                </label>
          </div>
        </div>

        

        <div className="cardcontent">
          <div class="form-check">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="flexCheckDefault"
            />
            <label class="form-check-label" for="flexCheckDefault">
Gold
            </label>
            <label className="loss">
                  -0.8%
                </label>
          </div>
        </div>
      </Card>
    </div>
  );
}
}

export default Commodities;

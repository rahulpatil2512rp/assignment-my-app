import "./App.css";
import React, { Component } from 'react';
import reportWebVitals from "./reportWebVitals";
import { AiTwotoneSetting } from "react-icons/ai";
import { GrFormClose, GrAdd } from "react-icons/gr";
import { FaChalkboardTeacher,FaLongArrowAltUp } from "react-icons/fa";


  class Watchlist extends React.Component {
    render()
{
  return (
    <div className="watchlistcard" style={{position:"absolute", marginLeft : "95rem"}}>
      <div class="card" style={{ width: "22rem", height: "56.2rem" }}>
        <div style={{position : "absolute", left:"1rem", top:"1rem"}}>
        <h5 style={{ color: "#000", fontWeight: "bold" }}>My Watchlist</h5>
        </div>

        <div class="card-body">
          <div
            style={{
              color: "black",
              fontSize: "18px",
              position: "absolute",
              top: "12px",
              left: "50%",
            }}
          >
            <AiTwotoneSetting />
          </div>
          <div
            style={{
              color: "black",
              fontSize: "30px",
              position: "absolute",
              top: "2px",
              left: "85%",
            }}
          >
            <GrFormClose />
          </div>

          <div className="dropdown">
          <div
            style={{
              color: "black",
              fontSize: "21px",
              position: "absolute",
              top: "36px",
              left: "0.5rem",
              paddingRight:"2rem"
            }}
          >
            <FaChalkboardTeacher />
          </div>
           <select className="dropdowncontent">
              <option className="dropdowncontent" value="#">
                {" "}
                Portfolio 1 (Created By Koyfin){" "}
              </option>
              <option className="dropdowncontent" value="#">
                Portfolio 1 (Created By Koyfin)
              </option>
              <option className="dropdowncontent" value="#">
                Portfolio 1 (Created By Koyfin)
              </option>
              <option className="dropdowncontent" value="#">
                Portfolio 1 (Created By Koyfin)
              </option>
              <option className="dropdowncontent" value="#">
                Portfolio 1 (Created By Koyfin)
              </option>
            </select>
          </div>
        </div>
        <div className="watchlistcardheader">
          <div className="watchlistcardcontent">
            <label class="form-check-label" for="flexCheckDefault">
              Security
            </label>
            <FaLongArrowAltUp style={{position:"absolute", marginLeft : "2.5rem", top:"0.9rem"}} />
            <label className="lastlabel">Last</label>
            <label className="percentage">1D%</label>
          </div>
        </div>

        <div className="scroller">
          <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="profit" style={{border:"none"}}>4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
          </div>
          <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="profit" style={{border:"none"}}>4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="loss" style={{border:"none"}}>-4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="loss" style={{border:"none"}}>-4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="profit" style={{border:"none"}}>4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="profit" style={{border:"none"}}>4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="loss" style={{border:"none"}}>-4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="profit" style={{border:"none"}}>4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="loss" style={{border:"none"}}>-4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="profit" style={{border:"none"}}>4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="loss" style={{border:"none"}}>-4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="profit" style={{border:"none"}}>4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
            <div className="watchlistcardlistcontent" >
            <label className="watchlistcardlist" style={{border:"none"}}>scrollable</label>
            <label className="lastlabel" style={{border:"none"}}>129.61</label>
            <label className="profit" style={{border:"none"}}>4.98%</label>
            <div className="watchlistlabel" style={{border:"none"}} >
              <label style={{border:"none"}}>Some Texts Appears here</label>
            </div>
            </div>
        </div>

        <div className="watchlistcardFooter">
          <div className="watchlistcardcontent">
            <GrAdd />
            <label
              style={{ color: "black", fontSize: "15", marginLeft: "15px" }}
            >
              Add Security to Watchlist
            </label>
          </div>
        </div>
      </div>
    </div>
  );
}
}

export default Watchlist;

import logo from "./logo.svg";
import "./App.css";
import reportWebVitals from "./reportWebVitals";
import Maincard from "./Maincard";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, fapensquare } from "@fortawesome/free-solid-svg-icons";
import Watchlist from "./Watchlist";
import Sidenav from "./Sidenav";
import { MdFeedback, MdCropFree } from "react-icons/md";
import { FaUserCircle } from "react-icons/fa";

function App() {
  return (
    <div className="App">
      <div className="search" style={{ marginLeft: "22.5rem" }}>
        <input
          className="searchtext"
          type="text"
          placeholder="Search for name, ticket or function"
          style={{
            width: "100%",
            backgroundColor: "#404653",
            color: "#fff",
            outline: "none",
            borderStyle: "none",
            padding: "5px",
            paddingLeft: "35px",
            borderRadius: "5px",
            borderTop: "0.5px solid #555d6e",
            borderBottom: "0.5px solid #555d6e",
          }}
        />
        <div className="icons">
          <FontAwesomeIcon icon={faSearch} />
        </div>
        <div className="searchbarend">
          <label style={{ position: "absolute", left: "7px", top: "-3px" }}>
            /
          </label>
        </div>
      </div>
      <MdFeedback
        style={{
          position: "absolute",
          top: "10px",
          right: "17rem",
          fontSize: "20px",
          color: "white",
        }}
      />
      <label
        style={{
          position: "absolute",
          top: "7px",
          right: "12rem",
          fontSize: "16px",
          color: "white",
        }}
      >
        Feedback
      </label>
      <MdCropFree
        style={{
          position: "absolute",
          top: "10px",
          right: "8rem",
          fontSize: "20px",
          color: "white",
        }}
      />
      <FaUserCircle
        style={{
          position: "absolute",
          top: "10px",
          right: "4rem",
          fontSize: "25px",
          color: "white",
        }}
      />
      <Maincard />
      <div>
        <Watchlist />
      </div>
      <div>
        <Sidenav />
      </div>
    </div>
  );
}

export default App;
